//
//  EditViewController.swift
//  Assignment
//
//  Created by Dibin Varghees on 05/02/18.
//  Copyright © 2018 Dibin Varghees. All rights reserved.
//

import UIKit

class EditViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var submitButton: UIButton!

    @IBOutlet weak var updateButton: UIButton!
    
    @IBOutlet weak var removeButton: UIButton!
    
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var LastNameTextField: UITextField!
    
    @IBOutlet weak var address1TextField: UITextField!
    
    @IBOutlet weak var address2TextField: UITextField!
    
    @IBOutlet weak var pinCodeTextField: UITextField!
    
    @IBOutlet weak var mobileNumberTextField: UITextField!
    
    var recordId:Int64?
    
    var activeField: UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let theTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected(gesture:)))
        
        self.view.addGestureRecognizer(theTap)
        
        self.setCornerRadiousFor(button: submitButton)
        
        self.setCornerRadiousFor(button: updateButton)
        
        self.setCornerRadiousFor(button: removeButton)

    }

    override func viewWillAppear(_ animated: Bool) {
        
        if let record = self.recordId {
            
            if let fetchedRecord = CoreDataManager.sharedDBManager.fetchRecordForId(rId: record)
            {
                
                self.firstNameTextField.text = fetchedRecord.firstname
                
                self.LastNameTextField.text = fetchedRecord.lastname
                
                self.address1TextField.text = fetchedRecord.address1
                
                self.address2TextField.text = fetchedRecord.address2
                
                self.pinCodeTextField.text = fetchedRecord.pinCode
                
                self.mobileNumberTextField.text = fetchedRecord.mobile
                
                self.removeButton.isHidden = false
                
                self.submitButton.isHidden = true
                
                self.updateButton.isHidden = false
            }
        }
        else{
            
            self.removeButton.isHidden = true
            
            self.submitButton.isHidden = false
            
            self.updateButton.isHidden = true
            
        }
        
        
    }
    override func viewDidLayoutSubviews() {
        
            self.scrollView.contentSize = CGSize(width:  self.containerView.frame.size.width, height: self.stackView.frame.origin.y + self.stackView.frame.height)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification) {
        
        self.scrollView.isScrollEnabled = true
        
        var info = notification.userInfo!
        
        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        
        aRect.size.height -= keyboardSize!.height
        
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
        
        

    }
    
    @objc func keyboardWillHide(_ notification: NSNotification) {
        
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = true
    }
    
    @objc func tapDetected(gesture: UITapGestureRecognizer)
    {
        print("TapDetected")
        
        self.activeField?.endEditing(true)
        
    }

  func textFieldDidBeginEditing(_ textField: UITextField)
 {
    
    activeField = textField

    }
        
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        activeField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    textField.resignFirstResponder()
        
    return true
        
    }
    @IBAction func submit(_ sender: Any) {
        
        
        guard  let detail = self.getDetailModel() else {
            
            return
        }
        
        CoreDataManager.sharedDBManager.insertNewRecordfor(model: detail)
        
        self.showAlretWithBAckAction(text: "Record Added")

    }
    
    func getDetailModel() -> detailModel?
    {
        
        let detailModelObj =  detailModel()
        
        if let firstName = self.firstNameTextField.text
        {
            
            if firstName != ""
            {
                detailModelObj.firstName = firstName
            }
            else
            {
                
                self.showAlertWith(Message: "Please enter FirstName")
                
                return nil
            }
        }
        if let LastName = self.LastNameTextField.text
        {
            
            if LastName != ""
            {
                
                detailModelObj.lastName = LastName
            }
            else
            {
                self.showAlertWith(Message: "Please enter LastName")
                
                return nil
            }
            
            
        }
        if let add1 = self.address1TextField.text
        {
            if add1 != ""
            {
                
                detailModelObj.address1 = add1
                
            }else
            {
                
                self.showAlertWith(Message: "Please enter address")
                
                return nil
            }
            
        }
        if let add2 = self.address2TextField.text
        {
            if add2 != ""
            {
                detailModelObj.address2 = add2
                
            }
            else
            {
                
                self.showAlertWith(Message: "Please enter address")
                
                return nil
            }
            
        }
        
        if let pin = self.pinCodeTextField.text
        {
            
            if pin != ""
            {
                
                detailModelObj.pinCode = pin
                
            }else
            {
                self.showAlertWith(Message: "Please enter Pincode")
                
                return nil
            }
        }
        
        if let mob = self.mobileNumberTextField.text
        {
            
            if mob != ""
            {
                detailModelObj.phone = mob
                
            }else
            {
                
                self.showAlertWith(Message: "Please enter phone Number")
                
                return nil
            }
        }
        
        detailModelObj.timeStamp =  Date().toMillis()
        
        print(detailModelObj)
        
        return detailModelObj
    }
    func showAlertWith(Message:String)
    {
        
        let alertController = UIAlertController(title: "Alert", message: Message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        alertController.addAction(ok)
        
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    func showAlretWithBAckAction(text:String)
    {
        
        let alertController = UIAlertController(title: "Alert", message: text, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: { ok in
            
            self.navigationController?.popViewController(animated: true)
        })
        
        alertController.addAction(ok)
        
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    @IBAction func updateClicked(_ sender: Any) {
        
        guard  let detail = self.getDetailModel() else {
            
            return
        }
        CoreDataManager.sharedDBManager.updateRecordFor(rId: recordId!, model: detail)
        
        self.showAlretWithBAckAction(text: "Record updated")

    }
    @IBAction func removeClicked(_ sender: Any) {
        
        if let rId = self.recordId {
            
            CoreDataManager.sharedDBManager.removeRecordWith(recordId: rId)
            
            self.showAlretWithBAckAction(text: "Record removed")
           
        }
    }
    
    func setCornerRadiousFor(button:UIButton)
    {
        
        button.layer.cornerRadius = 5.0
    }
 
}
