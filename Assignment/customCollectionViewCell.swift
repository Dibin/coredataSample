//
//  customCollectionViewCell.swift
//  Assignment
//
//  Created by Dibin Varghees on 05/02/18.
//  Copyright © 2018 Dibin Varghees. All rights reserved.
//

import UIKit

protocol customCellDelegate {
    
    func editButtonClickedWith(cellid:Int64)
}

class customCollectionViewCell: UICollectionViewCell {
    
    var cellId:Int64!
    
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
   
    var delegate:customCellDelegate? = nil
    
    override func awakeFromNib() {
        
        self.layer.borderColor = UIColor.lightGray.cgColor
        
        self.layer.borderWidth = 0.5
        
        self.layer.cornerRadius = 5.0
    }
    func updateCell(detailObject:Details)
    {
        
        self.nameLabel.text = detailObject.firstname! + " " + detailObject.lastname!
        
        let addressStr = detailObject.address1! + "\n" + detailObject.address2! + "\nPinCode : " +  detailObject.pinCode! + "\nPhone : " + detailObject.mobile!
        
        self.addressLabel.text = addressStr
                
        self.cellId = detailObject.timeStamp
    }
    
    @IBAction func editClick(_ sender: UIButton) {
        
        if self.delegate != nil
        {
            delegate?.editButtonClickedWith(cellid: self.cellId)
        }
    }
}
