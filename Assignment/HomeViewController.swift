//
//  ViewController.swift
//  Assignment
//
//  Created by Dibin Varghees on 05/02/18.
//  Copyright © 2018 Dibin Varghees. All rights reserved.
//

import UIKit
import CoreData

class HomeViewController: UIViewController,customCellDelegate {
   
    var timer:Timer?
    
    var  searchText = ""
    
    var searchPredicate:NSPredicate!
    
    var closeButton:UIBarButtonItem!
    
    var searchButton :UIBarButtonItem!
    
    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var noContentView: UIView!
    
    @IBOutlet weak var detailsCollectionView: UICollectionView!
    
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!

    var blockOperations: [BlockOperation] = []

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.initializeFetchedResultsController()
        
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        closeButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(disableSearch))
        
        searchButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(enableSearch))
        
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = searchButton
        
        let theTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected(gesture:)))
        
        self.view.addGestureRecognizer(theTap)
        
        self.searchTextField.isEnabled = false
    }

    override func viewWillAppear(_ animated: Bool) {
        
        if let objects = fetchedResultsController.fetchedObjects?.count {
            
            print(objects)
            
            if objects > 0
            {
                noContentView.alpha = 0
                
            }
            else
            {
                noContentView.alpha = 1
                
            }
        }
    }
    func initializeFetchedResultsController() {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Details")
        
        let nameSort = NSSortDescriptor(key: "firstname", ascending: true,
                                         selector: #selector(NSString.localizedCaseInsensitiveCompare))
        
        let timeSort = NSSortDescriptor(key: "timeStamp", ascending: false)
        
        request.sortDescriptors = [nameSort,timeSort]
        
        let appdelegate = UIApplication.shared .delegate as! AppDelegate
        
        let moc =  appdelegate.persistentContainer.viewContext
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        
        fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }
    @objc func tapDetected(gesture: UITapGestureRecognizer)
    {
        print("TapDetected")
        
        self.searchTextField.endEditing(true)
        
    }
    @objc func enableSearch()
    {

        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = closeButton

        self.searchTextField.isEnabled = true
        
        self.searchTextField.becomeFirstResponder()

    }
    
    @objc func disableSearch()
    {
        
        searchTextField.text = ""
        
        self.searchTextField.isEnabled = false
        
        searchTextField.resignFirstResponder()
        
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = searchButton
        
        self.fetchedResultsController.fetchRequest.predicate = nil
        
        do{
            
            try self.fetchedResultsController.performFetch()
            
            self.detailsCollectionView.reloadData()
            
        }catch
        {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func editButtonClickedWith(cellid: Int64) {
        
        self.view.resignFirstResponder()
        
        let EditVC = self.storyboard?.instantiateViewController(withIdentifier: "EditViewControllerID") as! EditViewController
        
      EditVC.recordId = cellid
        
        self.navigationController?.pushViewController(EditVC, animated: true)
    }

    func pushToEditViewController()
    {
        
        self.view.resignFirstResponder()

        let addVC = self.storyboard?.instantiateViewController(withIdentifier: "EditViewControllerID") as! EditViewController
        
        self.navigationController?.pushViewController(addVC, animated: true)
    }
    
    @IBAction func add(_ sender: Any) {
        
        self.pushToEditViewController()
        
    }
    @IBAction func addNew(_ sender: Any) {
        
        self.pushToEditViewController()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if let searchtext = textField.text
        {
            
            self.searchText = searchtext
            
            if self.timer != nil
            {
                
                self.timer?.invalidate()
                
                self.timer = nil
                
            }
            
            timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector:#selector(self.searchForText) , userInfo: nil, repeats: false)
            
        }
    }
    
    @objc func searchForText()
    {
        
        if searchText != "" {
            
            searchPredicate = NSPredicate(format: "(firstname contains [cd] %@) || (lastname contains [cd] %@)", searchText,searchText)
            
            self.fetchedResultsController.fetchRequest.predicate = searchPredicate
            
            do{
                
                try self.fetchedResultsController.performFetch()
                
                self.detailsCollectionView.reloadData()
                
            }catch{
                
                
            }
            
        }
        
        
    }
}

extension HomeViewController: NSFetchedResultsControllerDelegate
{
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        blockOperations.removeAll(keepingCapacity: false)
    }
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        if type == NSFetchedResultsChangeType.insert {
            print("Insert Object: \(newIndexPath)")
            
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        
                        this.detailsCollectionView.insertItems(at: [newIndexPath!])
                        
                    }
                })
            )
        }
        else if type == NSFetchedResultsChangeType.update {
            print("Update Object: \(indexPath)")
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        
                        
                        this.detailsCollectionView.reloadItems(at: [indexPath!])
                    }
                })
            )
        }
        else if type == NSFetchedResultsChangeType.move {
            print("Move Object: \(indexPath)")
            
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        
                        this.detailsCollectionView.moveItem(at: indexPath!, to: newIndexPath!)
                    }
                })
            )
        }
        else if type == NSFetchedResultsChangeType.delete {
            print("Delete Object: \(indexPath)")
            
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        
                        this.detailsCollectionView.deleteItems(at: [indexPath!])
                    }
                })
            )
        }
    }
    
    func controller(controller: NSFetchedResultsController<NSFetchRequestResult>, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        
        if type == NSFetchedResultsChangeType.insert {
            print("Insert Section: \(sectionIndex)")
            
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        
                        
                        this.detailsCollectionView.insertSections(NSIndexSet(index: sectionIndex) as IndexSet)
                        
                    }
                })
            )
        }
        else if type == NSFetchedResultsChangeType.update {
            print("Update Section: \(sectionIndex)")
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        
                        
                        this.detailsCollectionView.reloadSections(NSIndexSet(index: sectionIndex) as IndexSet)
                    }
                })
            )
        }
        else if type == NSFetchedResultsChangeType.delete {
            print("Delete Section: \(sectionIndex)")
            
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        
                        this.detailsCollectionView.deleteSections(NSIndexSet(index: sectionIndex) as IndexSet)
                    }
                })
            )
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        if #available(iOS 11.0, *) {
            detailsCollectionView.performBatchUpdates({ () -> Void in
                for operation: BlockOperation in self.blockOperations {
                    operation.start()
                }
            }, completion: { (finished) -> Void in
                self.blockOperations.removeAll(keepingCapacity: false)
            })
        } else {
            // Fallback on earlier versions
        }
    }
    
    func configureCell(cell: customCollectionViewCell, indexPath: NSIndexPath) {
        guard let selectedObject = fetchedResultsController.object(at: indexPath as IndexPath) as? Details else { fatalError("Unexpected Object in FetchedResultsController") }
        
        cell.updateCell(detailObject: selectedObject)
        
        // Populate cell from the NSManagedObject instance
        print("Object for configuration: \(selectedObject)")
    }
}

extension HomeViewController : UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        guard let sections = fetchedResultsController.sections else {
            fatalError("No sections in fetchedResultsController")
        }
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! customCollectionViewCell
        
        self.configureCell(cell: cell, indexPath: indexPath as NSIndexPath)
        
        cell.delegate = self
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.view.frame.width - 20 , height: 130)
    }
    
    
}

