//
//  CoreDataManager.swift
//  
//
//  Created by Dibin on 5/01/18.
//
//

import UIKit
import CoreData

class CoreDataManager: NSObject {
    
      static  let sharedDBManager = CoreDataManager()
        
      func getManagedObjectContext() -> (NSManagedObjectContext) {
        
        let theAppDelegate:AppDelegate = (UIApplication.shared).delegate as! AppDelegate
        
        let context:NSManagedObjectContext = theAppDelegate.persistentContainer.viewContext
        
        return context
        
    }
    
    func insertNewRecordfor(model: detailModel)  {
        
        let context:NSManagedObjectContext = self.getManagedObjectContext()
        
        let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        
        privateContext.parent = context
        
        privateContext.performAndWait {
            
            // insert new record
            
            let detailDBmodel = NSEntityDescription.insertNewObject(forEntityName: "Details", into: privateContext) as! Details
            
            detailDBmodel.firstname = model.firstName
            
            detailDBmodel.lastname = model.lastName
            
            detailDBmodel.address1 = model.address1
            
            detailDBmodel.address2 = model.address2
            
            detailDBmodel.mobile = model.phone
            
            detailDBmodel.pinCode = model.pinCode
            
            detailDBmodel.timeStamp = model.timeStamp
            
            do
            {
                try privateContext.save()
                
                print("privateContext saved")
                
                do{
                    
                    try context.save()
                    
                }catch let error as NSError{
                    
                    print("Error could not save feed\(error)")
                    
                }
                
            } catch let error as NSError{
                
                print("Error could not save feed\(error)")
                
            }
        }
        
    }

    func updateRecordFor(rId:Int64,model:detailModel)
    {
        
        // record already exists
        
        let context:NSManagedObjectContext = self.getManagedObjectContext()

        if let ObjectDB = self.fetchRecordForId(rId: rId )
        {
            
            ObjectDB.firstname = model.firstName
            
            ObjectDB.lastname = model.lastName
            
            ObjectDB.address1 = model.address1
            
            ObjectDB.address2 = model.address2
            
            ObjectDB.mobile = model.phone
            
            ObjectDB.pinCode = model.pinCode
            
            do{
                
                try context.save()
                
                print("Updated...")
                
            }catch let error as NSError{
                
                print("Error could not save feed\(error)")
                
            }
        }
    }

    func fetchRecordForId(rId:Int64) -> Details?
    {
        let context = self.getManagedObjectContext()
        
        let request = NSFetchRequest<Details>(entityName: "Details")
        
        let resultPredicate = NSPredicate(format: "timeStamp == %@","\(rId)")

        request.predicate = resultPredicate
        
        var resultObj:Details?
        
        do
        {
            
            let result = try context.fetch(request)
            
            
            if result.count > 0 {
                
                resultObj = result[0]
                
            }
            
        }catch let error as NSError
        {
            
            print("Error could not fetch\(error)")
            
        }
        
        return resultObj
    }

    func removeRecordWith(recordId:Int64)
    {
        
        let moc = self.getManagedObjectContext()
        
        if let obj = self.fetchRecordForId(rId: recordId)
        {
            
        moc.delete(obj)
            
            do{
                
                try moc.save()
                
                print("Updated...")
                
            }catch let error as NSError{
                
                print("Error could not save feed\(error)")
                
            }
        }
        
        
    }
}
extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
